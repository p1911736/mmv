#include "qte.h"
#include "implicits.h"
#include "ui_interface.h"
#include <iostream>
#include <chrono>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <random>
#include "../heightfield.h"
#include "../vegetation.h"

using namespace std;

QString HEIGHTMAP_1 = "heightmap1";
QString HEIGHTMAP_2 = "heightmap2";
QString HEIGHTMAP_3 = "heightmap3";
QString HEIGHTMAP_4 = "heightmap4";

MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);

	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(10, 0, 0), Vector(0.0, 0.0, 0.0)));
}

MainWindow::~MainWindow()
{
	delete meshWidget;
}

void MainWindow::CreateActions()
{
	connect(uiw->heightfield1, &QPushButton::clicked, [=]() {
		displayHeightField(HEIGHTMAP_1);
		});
	connect(uiw->heightfield2, &QPushButton::clicked, [=]() {
		displayHeightField(HEIGHTMAP_2);
		});
	connect(uiw->heightfield3, &QPushButton::clicked, [=]() {
		displayHeightField(HEIGHTMAP_3);
		});
	connect(uiw->heightfield4, &QPushButton::clicked, [=]() {
		displayHeightField(HEIGHTMAP_4);
		});
	connect(uiw->gradientNorm, SIGNAL(clicked()), this, SLOT(saveGradientNormImage()));
	connect(uiw->laplacian, SIGNAL(clicked()), this, SLOT(saveLaplacianImage()));
	connect(uiw->smooth, SIGNAL(clicked()), this, SLOT(saveSmoothImage()));
	connect(uiw->blur, SIGNAL(clicked()), this, SLOT(saveBlurImage()));
	connect(uiw->clamp, SIGNAL(clicked()), this, SLOT(saveClampImage()));
	connect(uiw->shade, SIGNAL(clicked()), this, SLOT(saveShadeImage()));
	connect(uiw->toObj, SIGNAL(clicked()), this, SLOT(exportHeighFieldOBJ()));
	connect(uiw->streamarea, SIGNAL(clicked()), this, SLOT(saveStreamAreaImage()));
	connect(uiw->streampower, SIGNAL(clicked()), this, SLOT(saveStreamPowerImage()));
	connect(uiw->wetness, SIGNAL(clicked()), this, SLOT(saveWetnessImage()));
	connect(uiw->access, SIGNAL(clicked()), this, SLOT(saveAccessImage()));
	connect(uiw->area, &QCheckBox::stateChanged, this, &MainWindow::createBox);
	connect(uiw->forest, SIGNAL(clicked()), this, SLOT(buildVegetation()));
	connect(uiw->streampower2, SIGNAL(clicked()), this, SLOT(applyStreamPowerErosion()));
	connect(uiw->hill_hope, SIGNAL(clicked()), this, SLOT(applyHillHopeErosion()));
	connect(uiw->thermal, SIGNAL(clicked()), this, SLOT(applyThermalErosion()));

	connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
    connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
}

void MainWindow::displayHeightField(QString heightField_name)
{
	heightfield = new HeightField("Results/" + heightField_name + ".png", 1, 1);

	QString height_max_str = uiw->height_max->text();
	double height_max = height_max_str.toDouble();

	Mesh heightfieldMesh(heightfield, height_max);

	vector<Color> cols;
	cols.resize(heightfieldMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
	UpdateGeometry();

	heightfieldToObj = heightfield;
	nameObj = "heightmapObj";
	currentHeightFieldName = heightField_name;

	uiw->area_min_x->setText(QString::number(heightfield->a.data[1] + 2));
	uiw->area_min_y->setText(QString::number(heightfield->a.data[1] + 2));
	uiw->area_min_z->setText(QString::number(heightfield->a.data[1] + 2));

	uiw->area_max_x->setText(QString::number(heightfield->b.data[1] - 2));
	uiw->area_max_y->setText(QString::number(heightfield->b.data[1] - 2));
	uiw->area_max_z->setText(QString::number(heightfield->b.data[1] - 2));

}

void MainWindow::saveGradientNormImage()
{
	if (heightfield != nullptr)
	{
		ScalarField sf = heightfield->GradientNorm();
		sf.SaveAsImage("Results/gradientNorm/" + currentHeightFieldName + "/GradientNormColor.png", true);
		sf.SaveAsImage("Results/gradientNorm/" + currentHeightFieldName + "/GradientNormGray.png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf);
		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "gradientNormObj";
	}
}

void MainWindow::saveLaplacianImage()
{
	if (heightfield != nullptr)
	{
		ScalarField sf = heightfield->Laplacian();
		sf.SaveAsImage("Results/laplacian/" + currentHeightFieldName + "/LaplacianColor.png", true);
		sf.SaveAsImage("Results/laplacian/" + currentHeightFieldName + "/LaplacianGray.png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf);

		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "laplacianObj";
	}
}

void MainWindow::saveSmoothImage()
{
	if (heightfield != nullptr)
	{
		ScalarField sf = heightfield->Smooth();
		sf.SaveAsImage("Results/smooth/" + currentHeightFieldName + "/SmoothColor.png", true);
		sf.SaveAsImage("Results/smooth/" + currentHeightFieldName + "/SmoothGray.png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf);
		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "smoothObj";
	}
}

void MainWindow::saveBlurImage()
{
	if (heightfield != nullptr)
	{
		ScalarField sf = heightfield->Blur();
		sf.SaveAsImage("Results/blur/" + currentHeightFieldName + "/BlurColor.png", true);
		sf.SaveAsImage("Results/blur/" + currentHeightFieldName + "/BlurGray.png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf);
		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "blurObj";
	}
}

void MainWindow::saveClampImage()
{
	QString clamp_min_str = uiw->clamp_min->text();
	QString clamp_max_str = uiw->clamp_max->text();

	if (!clamp_min_str.isEmpty() && !clamp_max_str.isEmpty())
	{
		double clamp_min_val = clamp_min_str.toDouble();
		double clamp_max_val = clamp_max_str.toDouble();

		if (heightfield != nullptr)
		{
			ScalarField sf = heightfield->Clamp(clamp_min_val, clamp_max_val);

			sf.SaveAsImage("Results/clamp/" + currentHeightFieldName + "/Clamp_min=" + QString::number(clamp_min_val) + "_max=" +
				QString::number(clamp_max_val) + "Color.png", true);
			sf.SaveAsImage("Results/clamp/" + currentHeightFieldName + "/Clamp_min=" + QString::number(clamp_min_val) + "_max=" +
				QString::number(clamp_max_val) + "Gray.png", false);


			QString height_max_str = uiw->height_max->text();
			double height_max = height_max_str.toDouble();

			HeightField* hf = new HeightField(sf);
			Mesh heightfieldMesh(hf, height_max);

			vector<Color> cols;
			cols.resize(heightfieldMesh.Vertexes());
			for (size_t i = 0; i < cols.size(); i++)
				cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

			meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
			UpdateGeometry();

			heightfieldToObj = hf;
			nameObj = "clampObj_min=" + QString::number(clamp_min_val) + "_max=" + QString::number(clamp_max_val);
		}
	}
}

void MainWindow::saveShadeImage()
{
	QString shade_x_str = uiw->lightShade_x->text();
	QString shade_y_str = uiw->lightShade_y->text();
	QString shade_z_str = uiw->lightShade_z->text();

	if (!shade_x_str.isEmpty() && !shade_y_str.isEmpty() && !shade_z_str.isEmpty())
	{
		double shade_x_val = shade_x_str.toDouble();
		double shade_y_val = shade_y_str.toDouble();
		double shade_z_val = shade_z_str.toDouble();

		if (heightfield != nullptr)
		{
			heightfield->Shade("Results/shade/" + currentHeightFieldName + "/Shade_light={" + QString::number(shade_x_val) +
				"," + QString::number(shade_y_val) +
				"," + QString::number(shade_z_val) + "}.png",
				Vec3{ shade_x_val, shade_y_val, shade_z_val });
		}
	}
}

void MainWindow::saveStreamAreaImage()
{
	if (heightfield != nullptr)
	{
		QString threshold_str = uiw->threshold->text();
		double threshold = threshold_str.toDouble();

		ScalarField sf = heightfield->StreamArea();
		sf.SaveAsImageStreamArea("Results/streamArea/" + currentHeightFieldName + "/StreamAreaColor_threshold=" + QString::number(threshold) + ".png", threshold, true);
		sf.SaveAsImageStreamArea("Results/streamArea/" + currentHeightFieldName + "/streamAreaGray_threshold=" + QString::number(threshold) + ".png", threshold, false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf.Normalize().ApplyThreshold(threshold));

		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "streamArea";
	}
}

void MainWindow::saveStreamPowerImage()
{
	if (heightfield != nullptr)
	{
		ScalarField sf = heightfield->StreamPower();
		sf.SaveAsImageStreamPower("Results/streamPower/" + currentHeightFieldName + "/StreamPowerColor.png", true);
		sf.SaveAsImageStreamPower("Results/streamPower/" + currentHeightFieldName + "/StreamPowerGray.png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf.Normalize());
		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "streamPower";

	}
}

void MainWindow::saveWetnessImage()
{
	if (heightfield != nullptr)
	{
		ScalarField sf = heightfield->WetnessIndex();
		sf.SaveAsImageStreamPower("Results/wetness/" + currentHeightFieldName + "/WetnessColor.png", true);
		sf.SaveAsImageStreamPower("Results/wetness/" + currentHeightFieldName + "/WetnessGray.png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();


		HeightField* hf = new HeightField(sf);
		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "wetness";
	}
}

void MainWindow::saveAccessImage()
{
	if (heightfield != nullptr)
	{
		QString num_rays_str = uiw->num_rays->text();
		double num_rays = num_rays_str.toDouble();

		auto start = std::chrono::high_resolution_clock::now();

		ScalarField sf = heightfield->Access(num_rays);

		auto stop = chrono::high_resolution_clock::now();
		auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);

		cout << "Access | Temps pour " << num_rays << " rayons : " << duration.count() << " millisecondes." << endl;

		sf.SaveAsImageAccess("Results/access/" + currentHeightFieldName + "/AccessColor_num_rays=" + QString::number(num_rays) + ".png", true);
		sf.SaveAsImageAccess("Results/access/" + currentHeightFieldName + "/AccessGray_num_rays=" + QString::number(num_rays) + ".png", false);

		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		HeightField* hf = new HeightField(sf);
		Mesh heightfieldMesh(hf, height_max);

		vector<Color> cols;
		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();

		heightfieldToObj = hf;
		nameObj = "access";
	}
}

void MainWindow::createBox(int state)
{
	if (state == Qt::Checked) {
		QString area_min_x_str = uiw->area_min_x->text();
		double area_min_x = area_min_x_str.toDouble();

		QString area_min_y_str = uiw->area_min_y->text();
		double area_min_y = area_min_y_str.toDouble();

		QString area_min_z_str = uiw->area_min_z->text();
		double area_min_z = area_min_z_str.toDouble();

		QString area_max_x_str = uiw->area_max_x->text();
		double area_max_x = area_max_x_str.toDouble();

		QString area_max_y_str = uiw->area_max_y->text();
		double area_max_y = area_max_y_str.toDouble();

		QString area_max_z_str = uiw->area_max_z->text();
		double area_max_z = area_max_z_str.toDouble();

		QString height_max_heightfield_str = uiw->height_max->text();
		double height_max_heightfield = height_max_heightfield_str.toDouble();

		Mesh boxMesh(area_min_x, area_min_y, area_min_z, area_max_x, area_max_y, area_max_z, heightfield, height_max_heightfield);

		vector<Color> cols;

		cols.resize(boxMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(1.0, 1.0, 1.0);

		meshColor = MeshColor(boxMesh, cols, boxMesh.VertexIndexes());
		UpdateGeometry();
	}
	else
	{
		QString height_max_heightfield_str = uiw->height_max->text();
		double height_max_heightfield = height_max_heightfield_str.toDouble();

		Mesh heightfieldMesh(heightfield, height_max_heightfield);

		vector<Color> cols;

		cols.resize(heightfieldMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(1.0, 1.0, 1.0);

		meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
		UpdateGeometry();
	}

}

void MainWindow::buildVegetation()
{
	QString area_min_x_str = uiw->area_min_x->text();
	double area_min_x = area_min_x_str.toDouble();

	QString area_min_y_str = uiw->area_min_y->text();
	double area_min_y = area_min_y_str.toDouble();

	QString area_min_z_str = uiw->area_min_z->text();
	double area_min_z = area_min_z_str.toDouble();

	QString area_max_x_str = uiw->area_max_x->text();
	double area_max_x = area_max_x_str.toDouble();

	QString area_max_y_str = uiw->area_max_y->text();
	double area_max_y = area_max_y_str.toDouble();

	QString area_max_z_str = uiw->area_max_z->text();
	double area_max_z = area_max_z_str.toDouble();

	QString nb_trees_str = uiw->nb_trees->text();
	int nb_trees = nb_trees_str.toInt();

	QString min_distance_str = uiw->min_distance->text();
	double min_distance = min_distance_str.toDouble();

	QString height_min_str = uiw->tree_min->text();
	double height_min = height_min_str.toDouble();

	QString height_max_str = uiw->tree_max->text();
	double height_max = height_max_str.toDouble();

	QString width_min_str = uiw->width_min->text();
	double width_min = width_min_str.toDouble();

	QString width_max_str = uiw->width_max->text();
	double width_max = width_max_str.toDouble();

	QString height_max_heightfield_str = uiw->height_max->text();
	double height_max_heightfield = height_max_heightfield_str.toDouble();

	Vegetation v(heightfield);
	vector<Tree> trees = v.BuildForest(Vec3{ area_min_x, area_min_y, area_min_z }, Vec3{ area_max_x, area_max_y, area_max_z }, nb_trees, min_distance, height_min, height_max, width_min, width_max);

	default_random_engine generator;
	uniform_real_distribution<double> distribution(0.0, 1.0);

	Mesh forestMesh(trees, heightfield, height_max_heightfield);

	vector<Color> cols;
	
	cols.resize(forestMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(forestMesh, cols, forestMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::applyStreamPowerErosion()
{
	QString iterations_str = uiw->iterations->text();
	double iterations = iterations_str.toDouble();

	double deltaTime = 0.01;

	double dt = 1;

	auto start = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < iterations; ++i)
	{
		heightfield->streamPowerErosion(dt);
		dt += deltaTime;
	}

	auto stop = chrono::high_resolution_clock::now();
	auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);

	cout << "Stream Power | Temps pour " << iterations << " iterations : " << duration.count() << " millisecondes." << endl;

	QString height_max_str = uiw->height_max->text();
	double height_max = height_max_str.toDouble();

	Mesh heightfieldMesh(heightfield, height_max);

	vector<Color> cols;
	cols.resize(heightfieldMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
	UpdateGeometry();

	heightfieldToObj = heightfield;
	nameObj = "streamPowerErosionObj";
}

void MainWindow::applyHillHopeErosion()
{
	QString iterations_str = uiw->iterations->text();
	double iterations = iterations_str.toDouble();

	double deltaTime = 0.01;
	
	double dt = 1;

	auto start = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < iterations; ++i)
	{
		heightfield->hillSlopeErosion(dt);
		dt += deltaTime;
	}

	auto stop = chrono::high_resolution_clock::now();
	auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);

	cout << "Hill Slope | Temps pour " << iterations << " iterations : " << duration.count() << " millisecondes." << endl;

	QString height_max_str = uiw->height_max->text();
	double height_max = height_max_str.toDouble();

	Mesh heightfieldMesh(heightfield, height_max);

	vector<Color> cols;
	cols.resize(heightfieldMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
	UpdateGeometry();

	heightfieldToObj = heightfield;
	nameObj = "hillHoperErosionObj";
}

void MainWindow::applyThermalErosion()
{
	QString iterations_str = uiw->iterations->text();
	double iterations = iterations_str.toDouble();

	double deltaTime = 0.01;

	QString angle_str = uiw->angle->text();
	double angle = angle_str.toDouble();

	double dt = 1;

	auto start = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < iterations; ++i)
	{
		heightfield->thermalErosion(dt, angle);
		dt += deltaTime;
	}

	auto stop = chrono::high_resolution_clock::now();
	auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);

	cout << "Thermal | Temps pour " << iterations << " iterations : " << duration.count() << " millisecondes." << endl;

	QString height_max_str = uiw->height_max->text();
	double height_max = height_max_str.toDouble();

	Mesh heightfieldMesh(heightfield, height_max);

	vector<Color> cols;
	cols.resize(heightfieldMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(heightfieldMesh, cols, heightfieldMesh.VertexIndexes());
	UpdateGeometry();

	heightfieldToObj = heightfield;
	nameObj = "thermalErosionObj";
}


void MainWindow::exportHeighFieldOBJ()
{
	if (heightfield != nullptr)
	{
		QString height_max_str = uiw->height_max->text();
		double height_max = height_max_str.toDouble();

		heightfieldToObj->ScaleValues(height_max / 255.0);
		heightfieldToObj->Export("Results/obj/" + currentHeightFieldName + "/" + nameObj + "_heightmax=" + QString::number(height_max) + ".obj");
	}
}

void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}
