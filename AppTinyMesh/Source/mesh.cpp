#include "mesh.h"
#include "../heightfield.h"
#include "../vegetation.h"

/*!
\class Mesh mesh.h

\brief Core triangle mesh class.
*/

int Mesh::AddVertex(const Vector& vertex)
{
    vertices.push_back(vertex);
    return vertices.size() - 1;
}

int Mesh::AddNormal(const Vector& normal)
{
    normals.push_back(normal);
    return normals.size() - 1;
}

Mesh::Mesh(const HeightField* heightfield, const double& height_max)
{
    if (!heightfield) {
        return;
    }

    // Parcours de la heightfield pour crer les vertices et triangles du mesh
    for (int i = 0; i < heightfield->nx - 1; ++i) {
        for (int j = 0; j < heightfield->ny - 1; ++j) {
            // Obtenez les vertices pour chaque coin du quadrilatre
            Vec3 vertex0 = heightfield->Vertex(i, j);
            Vec3 vertex1 = heightfield->Vertex(i + 1, j);
            Vec3 vertex2 = heightfield->Vertex(i, j + 1);
            Vec3 vertex3 = heightfield->Vertex(i + 1, j + 1);

            // Obtenez les normales pour chaque coin du quadrilatre
            Vec3 normal0 = heightfield->Normal(i, j);
            Vec3 normal1 = heightfield->Normal(i + 1, j);
            Vec3 normal2 = heightfield->Normal(i, j + 1);
            Vec3 normal3 = heightfield->Normal(i + 1, j + 1);

            // Ajoutez les vertices  la liste du maillage et obtenez les indices
            const int v0 = AddVertex(Vector(vertex0.data[0], vertex0.data[1], (1 - vertex0.data[2] / 255.0) * height_max));
            const int v1 = AddVertex(Vector(vertex1.data[0], vertex1.data[1], (1 - vertex1.data[2] / 255.0) * height_max));
            const int v2 = AddVertex(Vector(vertex2.data[0], vertex2.data[1], (1 - vertex2.data[2] / 255.0) * height_max));
            const int v3 = AddVertex(Vector(vertex3.data[0], vertex3.data[1], (1 - vertex3.data[2] / 255.0) * height_max));

            // Ajoutez les normales  la liste du maillage et obtenez les indices
            const int n0 = AddNormal(Vector(normal0.data[0], normal0.data[1], normal0.data[2]));
            const int n1 = AddNormal(Vector(normal1.data[0], normal1.data[1], normal1.data[2]));
            const int n2 = AddNormal(Vector(normal2.data[0], normal2.data[1], normal2.data[2]));
            const int n3 = AddNormal(Vector(normal3.data[0], normal3.data[1], normal3.data[2]));

            // Ajoutez les indices des vertices et normales pour former un quadrilatre
            AddSmoothQuadrangle(v2, n2, v0, n0, v1, n1, v3, n3);
        }
    }
}

Mesh::Mesh(const vector<Tree>& trees, const HeightField* heightfield, const double& height_max)
{
    if (!heightfield) {
        return;
    }

    // Parcours de la heightfield pour crer les vertices et triangles du mesh
    for (int i = 0; i < heightfield->nx - 1; ++i) {
        for (int j = 0; j < heightfield->ny - 1; ++j) {
            // Obtenez les vertices pour chaque coin du quadrilatre
            Vec3 vertex0 = heightfield->Vertex(i, j);
            Vec3 vertex1 = heightfield->Vertex(i + 1, j);
            Vec3 vertex2 = heightfield->Vertex(i, j + 1);
            Vec3 vertex3 = heightfield->Vertex(i + 1, j + 1);

            // Obtenez les normales pour chaque coin du quadrilatre
            Vec3 normal0 = heightfield->Normal(i, j);
            Vec3 normal1 = heightfield->Normal(i + 1, j);
            Vec3 normal2 = heightfield->Normal(i, j + 1);
            Vec3 normal3 = heightfield->Normal(i + 1, j + 1);

            // Ajoutez les vertices  la liste du maillage et obtenez les indices
            const int v0 = AddVertex(Vector(vertex0.data[0], vertex0.data[1], (1 - vertex0.data[2] / 255.0) * height_max));
            const int v1 = AddVertex(Vector(vertex1.data[0], vertex1.data[1], (1 - vertex1.data[2] / 255.0) * height_max));
            const int v2 = AddVertex(Vector(vertex2.data[0], vertex2.data[1], (1 - vertex2.data[2] / 255.0) * height_max));
            const int v3 = AddVertex(Vector(vertex3.data[0], vertex3.data[1], (1 - vertex3.data[2] / 255.0) * height_max));

            // Ajoutez les normales  la liste du maillage et obtenez les indices
            const int n0 = AddNormal(Vector(normal0.data[0], normal0.data[1], normal0.data[2]));
            const int n1 = AddNormal(Vector(normal1.data[0], normal1.data[1], normal1.data[2]));
            const int n2 = AddNormal(Vector(normal2.data[0], normal2.data[1], normal2.data[2]));
            const int n3 = AddNormal(Vector(normal3.data[0], normal3.data[1], normal3.data[2]));

            // Ajoutez les indices des vertices et normales pour former un quadrilatre
            AddSmoothQuadrangle(v2, n2, v0, n0, v1, n1, v3, n3);
        }
    }

    // Pour chaque arbre
    for (const Tree& tree : trees) {
        // Calcul des demi-dimensions du prisme
        double halfWidth = tree.width / 2.0;
        double halfHeight = tree.height / 2.0;

        // Points du prisme
        Vec3 frontTopLeft{ tree.position[0] + halfWidth, tree.position[1] - halfWidth, ((1 - tree.position[2] / 255.0) * height_max) - tree.height };
        Vec3 frontTopRight{ tree.position[0] - halfWidth, tree.position[1] - halfWidth, ((1 - tree.position[2] / 255.0) * height_max) - tree.height };
        Vec3 frontBottomLeft{ tree.position[0] + halfWidth, tree.position[1] - halfWidth, (1 - tree.position[2] / 255.0) * height_max };
        Vec3 frontBottomRight{ tree.position[0] - halfWidth, tree.position[1] - halfWidth, (1 - tree.position[2] / 255.0) * height_max };

        Vec3 backTopLeft{ tree.position[0] + halfWidth, tree.position[1] + halfWidth, (1 - tree.position[2] / 255.0) * height_max - tree.height };
        Vec3 backTopRight{ tree.position[0] - halfWidth, tree.position[1] + halfWidth, (1 - tree.position[2] / 255.0) * height_max - tree.height };
        Vec3 backBottomLeft{ tree.position[0] + halfWidth, tree.position[1] + halfWidth, (1 - tree.position[2] / 255.0) * height_max };
        Vec3 backBottomRight{ tree.position[0] - halfWidth, tree.position[1] + halfWidth, (1 - tree.position[2] / 255.0) * height_max };

        // Normales du prisme
        Vec3 frontNormal{ 0.0, 0.0, -1.0 };
        Vec3 backNormal{ 0.0, 0.0, 1.0 };
        Vec3 leftNormal{ -1.0, 0.0, 0.0 };
        Vec3 rightNormal{ 1.0, 0.0, 0.0 };
        Vec3 topNormal{ 0.0, 1.0, 0.0 };
        Vec3 bottomNormal{ 0.0, -1.0, 0.0 };

        // Ajout des vertices et des normales
        const int v0 = AddVertex(Vector(frontTopLeft.data[0], frontTopLeft.data[1], frontTopLeft.data[2]));
        const int v1 = AddVertex(Vector(frontTopRight.data[0], frontTopRight.data[1], frontTopRight.data[2]));
        const int v2 = AddVertex(Vector(frontBottomLeft.data[0], frontBottomLeft.data[1], frontBottomLeft.data[2]));
        const int v3 = AddVertex(Vector(frontBottomRight.data[0], frontBottomRight.data[1], frontBottomRight.data[2]));

        const int v4 = AddVertex(Vector(backTopLeft.data[0], backTopLeft.data[1], backTopLeft.data[2]));
        const int v5 = AddVertex(Vector(backTopRight.data[0], backTopRight.data[1], backTopRight.data[2]));
        const int v6 = AddVertex(Vector(backBottomLeft.data[0], backBottomLeft.data[1], backBottomLeft.data[2]));
        const int v7 = AddVertex(Vector(backBottomRight.data[0], backBottomRight.data[1], backBottomRight.data[2]));

        const int nFront = AddNormal(Vector(frontNormal.data[0], frontNormal.data[1], frontNormal.data[2]));
        const int nBack = AddNormal(Vector(backNormal.data[0], backNormal.data[1], backNormal.data[2]));
        const int nLeft = AddNormal(Vector(leftNormal.data[0], leftNormal.data[1], leftNormal.data[2]));
        const int nRight = AddNormal(Vector(rightNormal.data[0], rightNormal.data[1], rightNormal.data[2]));
        const int nTop = AddNormal(Vector(topNormal.data[0], topNormal.data[1], topNormal.data[2]));
        const int nBottom = AddNormal(Vector(bottomNormal.data[0], bottomNormal.data[1], bottomNormal.data[2]));

        // Front
        AddSmoothQuadrangle(v2, nFront, v0, nFront, v1, nFront, v3, nFront);
        // Back
        AddSmoothQuadrangle(v4, nBack, v5, nBack, v7, nBack, v6, nBack);
        // Left
        AddSmoothQuadrangle(v2, nLeft, v6, nLeft, v4, nLeft, v0, nLeft);
        // Right
        AddSmoothQuadrangle(v1, nRight, v5, nRight, v7, nRight, v3, nRight);
        // Top
        AddSmoothQuadrangle(v0, nTop, v4, nTop, v5, nTop, v1, nTop);
        // Bottom
        AddSmoothQuadrangle(v6, nBottom, v2, nBottom, v3, nBottom, v7, nBottom);
    }
}

Mesh::Mesh(double area_min_x, double area_min_y, double area_min_z, double area_max_x, double area_max_y, double area_max_z, const HeightField* heightfield, const double& height_max)
{
    if (!heightfield) {
        return;
    }

    // Parcours de la heightfield pour crer les vertices et triangles du mesh
    for (int i = 0; i < heightfield->nx - 1; ++i) {
        for (int j = 0; j < heightfield->ny - 1; ++j) {
            // Obtenez les vertices pour chaque coin du quadrilatre
            Vec3 vertex0 = heightfield->Vertex(i, j);
            Vec3 vertex1 = heightfield->Vertex(i + 1, j);
            Vec3 vertex2 = heightfield->Vertex(i, j + 1);
            Vec3 vertex3 = heightfield->Vertex(i + 1, j + 1);

            // Obtenez les normales pour chaque coin du quadrilatre
            Vec3 normal0 = heightfield->Normal(i, j);
            Vec3 normal1 = heightfield->Normal(i + 1, j);
            Vec3 normal2 = heightfield->Normal(i, j + 1);
            Vec3 normal3 = heightfield->Normal(i + 1, j + 1);

            // Ajoutez les vertices  la liste du maillage et obtenez les indices
            const int v0 = AddVertex(Vector(vertex0.data[0], vertex0.data[1], (1 - vertex0.data[2] / 255.0) * height_max));
            const int v1 = AddVertex(Vector(vertex1.data[0], vertex1.data[1], (1 - vertex1.data[2] / 255.0) * height_max));
            const int v2 = AddVertex(Vector(vertex2.data[0], vertex2.data[1], (1 - vertex2.data[2] / 255.0) * height_max));
            const int v3 = AddVertex(Vector(vertex3.data[0], vertex3.data[1], (1 - vertex3.data[2] / 255.0) * height_max));

            // Ajoutez les normales  la liste du maillage et obtenez les indices
            const int n0 = AddNormal(Vector(normal0.data[0], normal0.data[1], normal0.data[2]));
            const int n1 = AddNormal(Vector(normal1.data[0], normal1.data[1], normal1.data[2]));
            const int n2 = AddNormal(Vector(normal2.data[0], normal2.data[1], normal2.data[2]));
            const int n3 = AddNormal(Vector(normal3.data[0], normal3.data[1], normal3.data[2]));

            // Ajoutez les indices des vertices et normales pour former un quadrilatre
            AddSmoothQuadrangle(v2, n2, v0, n0, v1, n1, v3, n3);
        }
    }

    // Points de la bo�te
    Vec3 boxMin{ area_min_x, area_min_y, area_min_z };
    Vec3 boxMax{ area_max_x, area_max_y, area_max_z };

    // Ajout des vertices et des normales
    const int v0 = AddVertex(Vector(boxMin.data[0], boxMin.data[1], boxMin.data[2]));
    const int v1 = AddVertex(Vector(boxMax.data[0], boxMin.data[1], boxMin.data[2]));
    const int v2 = AddVertex(Vector(boxMin.data[0], boxMax.data[1], boxMin.data[2]));
    const int v3 = AddVertex(Vector(boxMax.data[0], boxMax.data[1], boxMin.data[2]));

    const int v4 = AddVertex(Vector(boxMin.data[0], boxMin.data[1], boxMax.data[2]));
    const int v5 = AddVertex(Vector(boxMax.data[0], boxMin.data[1], boxMax.data[2]));
    const int v6 = AddVertex(Vector(boxMin.data[0], boxMax.data[1], boxMax.data[2]));
    const int v7 = AddVertex(Vector(boxMax.data[0], boxMax.data[1], boxMax.data[2]));

    const int nFront = AddNormal(Vector(0.0, 0.0, -1.0));
    const int nBack = AddNormal(Vector(0.0, 0.0, 1.0));
    const int nLeft = AddNormal(Vector(-1.0, 0.0, 0.0));
    const int nRight = AddNormal(Vector(1.0, 0.0, 0.0));
    const int nTop = AddNormal(Vector(0.0, 1.0, 0.0));
    const int nBottom = AddNormal(Vector(0.0, -1.0, 0.0));

    // Front
    AddSmoothQuadrangle(v2, nFront, v0, nFront, v1, nFront, v3, nFront);
    // Back
    AddSmoothQuadrangle(v4, nBack, v5, nBack, v7, nBack, v6, nBack);
    // Left
    AddSmoothQuadrangle(v2, nLeft, v6, nLeft, v4, nLeft, v0, nLeft);
    // Right
    AddSmoothQuadrangle(v1, nRight, v5, nRight, v7, nRight, v3, nRight);
    // Top
    AddSmoothQuadrangle(v0, nTop, v4, nTop, v5, nTop, v1, nTop);
    // Bottom
    AddSmoothQuadrangle(v6, nBottom, v2, nBottom, v3, nBottom, v7, nBottom);
}



/*!
\brief Initialize the mesh to empty.
*/
Mesh::Mesh()
{
}

/*!
\brief Initialize the mesh from a list of vertices and a list of triangles.

Indices must have a size multiple of three (three for triangle vertices and three for triangle normals).

\param vertices List of geometry vertices.
\param indices List of indices wich represent the geometry triangles.
*/
Mesh::Mesh(const std::vector<Vector>& vertices, const std::vector<int>& indices) :vertices(vertices), varray(indices)
{
    normals.resize(vertices.size(), Vector::Z);
}

/*!
\brief Create the mesh.

\param vertices Array of vertices.
\param normals Array of normals.
\param va, na Array of vertex and normal indexes.
*/
Mesh::Mesh(const std::vector<Vector>& vertices, const std::vector<Vector>& normals, const std::vector<int>& va, const std::vector<int>& na) :vertices(vertices), normals(normals), varray(va), narray(na)
{
}

/*!
\brief Reserve memory for arrays.
\param nv,nn,nvi,nvn Number of vertices, normals, vertex indexes and vertex normals.
*/
void Mesh::Reserve(int nv, int nn, int nvi, int nvn)
{
    vertices.reserve(nv);
    normals.reserve(nn);
    varray.reserve(nvi);
    narray.reserve(nvn);
}

/*!
\brief Empty
*/
Mesh::~Mesh()
{
}

/*!
\brief Smooth the normals of the mesh.

This function weights the normals of the faces by their corresponding area.
\sa Triangle::AreaNormal()
*/
void Mesh::SmoothNormals()
{
    // Initialize 
    normals.resize(vertices.size(), Vector::Null);

    narray = varray;

    // Accumulate normals
    for (int i = 0; i < varray.size(); i += 3)
    {
        Vector tn = Triangle(vertices[varray.at(i)], vertices[varray.at(i + 1)], vertices[varray.at(i + 2)]).AreaNormal();
        normals[narray[i + 0]] += tn;
        normals[narray[i + 1]] += tn;
        normals[narray[i + 2]] += tn;
    }

    // Normalize 
    for (int i = 0; i < normals.size(); i++)
    {
        Normalize(normals[i]);
    }
}

/*!
\brief Add a smooth triangle to the geometry.
\param a, b, c Index of the vertices.
\param na, nb, nc Index of the normals.
*/
void Mesh::AddSmoothTriangle(int a, int na, int b, int nb, int c, int nc)
{
    varray.push_back(a);
    narray.push_back(na);

    varray.push_back(b);
    narray.push_back(nb);
    varray.push_back(c);
    narray.push_back(nc);
}

/*!
\brief Add a triangle to the geometry.
\param a, b, c Index of the vertices.
\param n Index of the normal.
*/
void Mesh::AddTriangle(int a, int b, int c, int n)
{
    varray.push_back(a);
    narray.push_back(n);
    varray.push_back(b);
    narray.push_back(n);
    varray.push_back(c);
    narray.push_back(n);
}

/*!
\brief Add a smmoth quadrangle to the geometry.

Creates two smooth triangles abc and acd.

\param a, b, c, d  Index of the vertices.
\param na, nb, nc, nd Index of the normal for all vertices.
*/
void Mesh::AddSmoothQuadrangle(int a, int na, int b, int nb, int c, int nc, int d, int nd)
{
    // First triangle
    AddSmoothTriangle(a, na, b, nb, c, nc);

    // Second triangle
    AddSmoothTriangle(a, na, c, nc, d, nd);
}

/*!
\brief Add a quadrangle to the geometry.

\param a, b, c, d  Index of the vertices and normals.
*/
void Mesh::AddQuadrangle(int a, int b, int c, int d)
{
    AddSmoothQuadrangle(a, a, b, b, c, c, d, d);
}


/*!
\brief Compute the bounding box of the object.
*/
Box Mesh::GetBox() const
{
    if (vertices.size() == 0)
    {
        return Box::Null;
    }
    return Box(vertices);
}

/*!
\brief Creates an axis aligned box.

The object has 8 vertices, 6 normals and 12 triangles.
\param box The box.
*/
Mesh::Mesh(const Box& box)
{
    // Vertices
    vertices.resize(8);

    for (int i = 0; i < 8; i++)
    {
        vertices[i] = box.Vertex(i);
    }

    // Normals
    normals.push_back(Vector(-1, 0, 0));
    normals.push_back(Vector(1, 0, 0));
    normals.push_back(Vector(0, -1, 0));
    normals.push_back(Vector(0, 1, 0));
    normals.push_back(Vector(0, 0, -1));
    normals.push_back(Vector(0, 0, 1));

    // Reserve space for the triangle array
    varray.reserve(12 * 3);
    narray.reserve(12 * 3);

    AddTriangle(0, 2, 1, 4);
    AddTriangle(1, 2, 3, 4);

    AddTriangle(4, 5, 6, 5);
    AddTriangle(5, 7, 6, 5);

    AddTriangle(0, 4, 2, 0);
    AddTriangle(4, 6, 2, 0);

    AddTriangle(1, 3, 5, 1);
    AddTriangle(3, 7, 5, 1);

    AddTriangle(0, 1, 5, 2);
    AddTriangle(0, 5, 4, 2);

    AddTriangle(3, 2, 7, 3);
    AddTriangle(6, 7, 2, 3);
}

/*!
\brief Scale the mesh.
\param s Scaling factor.
*/
void Mesh::Scale(double s)
{
    // Vertexes
    for (int i = 0; i < vertices.size(); i++)
    {
        vertices[i] *= s;
    }

    if (s < 0.0)
    {
        // Normals
        for (int i = 0; i < normals.size(); i++)
        {
            normals[i] = -normals[i];
        }
    }
}



#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QRegularExpression>
#include <QtCore/qstring.h>

/*!
\brief Import a mesh from an .obj file.
\param filename File name.
*/
void Mesh::Load(const QString& filename)
{
    vertices.clear();
    normals.clear();
    varray.clear();
    narray.clear();

    QFile data(filename);

    if (!data.open(QFile::ReadOnly))
        return;
    QTextStream in(&data);

    // Set of regular expressions : Vertex, Normal, Triangle
    QRegularExpression rexv("v\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)");
    QRegularExpression rexn("vn\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)");
    QRegularExpression rext("f\\s*(\\d*)/\\d*/(\\d*)\\s*(\\d*)/\\d*/(\\d*)\\s*(\\d*)/\\d*/(\\d*)");
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QRegularExpressionMatch match = rexv.match(line);
        QRegularExpressionMatch matchN = rexn.match(line);
        QRegularExpressionMatch matchT = rext.match(line);
        if (match.hasMatch())//rexv.indexIn(line, 0) > -1)
        {
            Vector q = Vector(match.captured(1).toDouble(), match.captured(2).toDouble(), match.captured(3).toDouble()); vertices.push_back(q);
        }
        else if (matchN.hasMatch())//rexn.indexIn(line, 0) > -1)
        {
            Vector q = Vector(matchN.captured(1).toDouble(), matchN.captured(2).toDouble(), matchN.captured(3).toDouble());  normals.push_back(q);
        }
        else if (matchT.hasMatch())//rext.indexIn(line, 0) > -1)
        {
            varray.push_back(matchT.captured(1).toInt() - 1);
            varray.push_back(matchT.captured(3).toInt() - 1);
            varray.push_back(matchT.captured(5).toInt() - 1);
            narray.push_back(matchT.captured(2).toInt() - 1);
            narray.push_back(matchT.captured(4).toInt() - 1);
            narray.push_back(matchT.captured(6).toInt() - 1);
        }
    }
    data.close();
}

/*!
\brief Save the mesh in .obj format, with vertices and normals.
\param url Filename.
\param meshName %Mesh name in .obj file.
*/
void Mesh::SaveObj(const QString& url, const QString& meshName) const
{
    QFile data(url);
    if (!data.open(QFile::WriteOnly))
        return;
    QTextStream out(&data);
    out << "g " << meshName << Qt::endl;
    for (int i = 0; i < vertices.size(); i++)
        out << "v " << vertices.at(i)[0] << " " << vertices.at(i)[1] << " " << vertices.at(i)[2] << QString('\n');
    for (int i = 0; i < normals.size(); i++)
        out << "vn " << normals.at(i)[0] << " " << normals.at(i)[1] << " " << normals.at(i)[2] << QString('\n');
    for (int i = 0; i < varray.size(); i += 3)
    {
        out << "f " << varray.at(i) + 1 << "//" << narray.at(i) + 1 << " "
            << varray.at(i + 1) + 1 << "//" << narray.at(i + 1) + 1 << " "
            << varray.at(i + 2) + 1 << "//" << narray.at(i + 2) + 1 << " "
            << "\n";
    }
    out.flush();
    data.close();
}

