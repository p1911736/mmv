#ifndef SCALARFIELD_H
#define SCALARFIELD_H

#include "grid.h"
#include "vec.h"

using namespace std;

class ScalarField : public Grid {
public:
    ScalarField();
    ScalarField(const Vec2& a, const Vec2& b, int nx, int ny, vector<double> values_);
    ScalarField(const Vec2& a, const Vec2& b, int nx, int ny);

    void SetValue(int i, int j, double value);
    double GetValue(int i, int j) const;

    void SaveAsImage(const QString& fileName, bool color = true) const;
    void SaveAsImageStreamArea(const QString& fileName, const double& threshold, bool color = true) const;
    void SaveAsImageStreamPower(const QString& fileName, bool color = true) const;
    void SaveAsImageAccess(const QString& fileName, bool color) const;

    Vec2 Gradient(int i, int j) const;

    ScalarField GradientNorm() const;
    ScalarField Laplacian() const;

    ScalarField Smooth() const;
    ScalarField Blur() const;

    ScalarField Normalize() const;
    ScalarField Clamp(double minValue, double maxValue) const;
    ScalarField ApplyThreshold(double threshold) const;
    void ScaleValues(double scale);

    friend ostream& operator<<(ostream& os, const ScalarField& sf);

    vector<double> values;
};

#endif
