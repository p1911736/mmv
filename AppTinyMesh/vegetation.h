#ifndef VEGETATION_H
#define VEGETATION_H

#include "box2.h"
#include "heightfield.h"
#include "vec.h"
#include <vector>

using namespace std;

class Tree {
public:
    Tree(const Vec3& position, double hauteur, double witdh);

    Vec3 position;
    double height;
    double width;
};

class Vegetation {
public:
    Vegetation(HeightField* hf);

    Tree GeneratePoissonDiskTreeInBox(const Vec3& minBox, const Vec3& maxBox, double& minDistance, vector<Tree>& trees, double& height_min, double& height_max, double& width_min, double& width_max) const;
    vector<Tree> GeneratePoissonDisktrees(const Vec3& area_min, const Vec3& area_max, int& nbTrees, double& minDistance, double& height_min, double& height_max, double& width_min, double& width_max) const;
    vector<Tree> BuildForest(Vec3 area_min, Vec3 area_max, int nbTrees, double& minDistance, double& height_min, double& height_max, double& width_min, double& width_max) const;

    HeightField* heightfield;
};

#endif