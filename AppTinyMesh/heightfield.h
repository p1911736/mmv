#ifndef HEIGHTFIELD_H
#define HEIGHTFIELD_H

#include "scalarfield.h"

class HeightField : public ScalarField {
public:
    HeightField();
    HeightField(const ScalarField& sf);
    HeightField(const Vec2& a, const Vec2& b, int nx, int ny);
    HeightField(const QString& heightMapFileName, int dx_, int dy_);

    double Height(int i, int j) const;
    double Slope(int i, int j) const;
    double AverageSlope(int i, int j) const;

    Vec3 Vertex(int i, int j) const;
    Vec3 Normal(int i, int j) const;

    void Shade(const QString& fileName, const Vec3& lightDirection) const;
    void Export(const QString& fileName) const;

    ScalarField StreamArea() const;
    ScalarField StreamPower() const;
    ScalarField WetnessIndex() const;
    bool Intersect(const Ray& ray, double& t, double R) const;
    double CalculateMaxSlope() const;
    vector<Ray> createRaysRandomHemisphere(const Vec3& normale, int numRays, int i, int j) const;
    ScalarField Access(int numRays) const;

    void streamPowerErosion(double deltaTime);
    void hillSlopeErosion(double deltaTime);
    double slopeNeighbor(const int& i, const int& j, int offset_i, int offset_j) const;
    double maxSlope(const int& id_i, const int& id_j, int& i_neighbor, int& j_neighbor) const;
    void thermalErosion(double deltaTime, double angle);

    double dx, dy;
    double epsilon = 1e-6;
};

#endif // HEIGHTFIELD_H
