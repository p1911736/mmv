#include "vegetation.h"
#include <random>
#include <iostream>
#include <cmath>

default_random_engine generator;
uniform_real_distribution<double> distribution(0.0, 1.0);

Tree::Tree(const Vec3& position, double height, double width) : position(position), height(height), width(width) {}

Vegetation::Vegetation(HeightField* hf) : heightfield(hf) {}

Tree Vegetation::GeneratePoissonDiskTreeInBox(const Vec3& minBox, const Vec3& maxBox, double& minDistance, vector<Tree>& trees, double& height_min, double& height_max, double& width_min, double& width_max) const {
    for (int iterations = 0; iterations < 20; ++iterations) {
        double angle = 2.0 * M_PI * distribution(generator);
        double distance = minDistance + distribution(generator) * minDistance;

        Vec3 treePosition{
            round(distribution(generator) * (maxBox[0] - minBox[0]) + minBox[0]),
            round(distribution(generator) * (maxBox[1] - minBox[1]) + minBox[1]),
            0.0
        };

        treePosition[2] = heightfield->Height(treePosition[0], treePosition[1]);

        const double randomHeight = height_min + distribution(generator) * (height_max - height_min);
        const double randomWidth = width_min + distribution(generator) * (width_max - width_min);
        Tree newTree(treePosition, randomHeight, randomWidth);

        // V�rifier si le nouvel arbre est � une certaine distance des autres
        bool isValid = true;
        for (const Tree& existingTree : trees) {
            double distanceSq = (newTree.position - existingTree.position).LengthSquared();
            if (distanceSq < minDistance * minDistance) {
                isValid = false;
                break;
            }
        }

        if (isValid) {
            return newTree;
        }
    }

    // Si on ne trouve pas un arbre valide apr�s plusieurs tentatives
    return Tree(Vec3(), 0.0, 0.0);
}

vector<Tree> Vegetation::GeneratePoissonDisktrees(const Vec3& area_min, const Vec3& area_max, int& nbTrees, double& minDistance, double& height_min, double& height_max, double& width_min, double& width_max) const {
    generator.seed(26);

    // Choisir un point al�atoire � l'int�rieur de la bo�te
    vector<Tree> trees;
    
    const Tree initialTreeObj = GeneratePoissonDiskTreeInBox(area_min, area_max, minDistance, trees, height_min, height_max, width_min, width_max);
    if (initialTreeObj.height != 0.0) {
        trees.push_back(initialTreeObj);
    }

    while (trees.size() < nbTrees) {
        const int randomIndex = uniform_int_distribution<int>(0, trees.size() - 1)(generator);
        const Tree randomTree = trees[randomIndex];

        // G�n�rer un nouvel arbre autour du point choisi
        const Tree newTree = GeneratePoissonDiskTreeInBox(area_min, area_max, minDistance, trees, height_min, height_max, width_min, width_max);

        // S'il y a un nouvel arbre, on l'ajoute au tableau
        if (newTree.height != 0.0 && newTree.width != 0.0) {
            trees.push_back(newTree);
        }
        else {
            // Si on ne peut pas g�n�rer un nouvel arbre apr�s plusieurs tentatives, changer le point initial
            trees.clear();
            Vec3 initialTree{
                distribution(generator) * (area_max[0] - area_min[0]) + area_min[0],
                distribution(generator) * (area_max[1] - area_min[1]) + area_min[1],
                0.0
            };
            initialTree[2] = heightfield->Height(initialTree[0], initialTree[1]);

            const Tree initialTreeObj = GeneratePoissonDiskTreeInBox(area_min, area_max, minDistance, trees, height_min, height_max, width_min, width_max);
            if (initialTreeObj.height != 0.0) {
                trees.push_back(initialTreeObj);
            }
        }
    }

    return trees;
}

vector<Tree> Vegetation::BuildForest(Vec3 area_min, Vec3 area_max, int nbTrees, double& minDistance, double& height_min, double& height_max, double& width_min, double& width_max) const {
    auto start = std::chrono::high_resolution_clock::now();

    vector<Tree> result = GeneratePoissonDisktrees(area_min, area_max, nbTrees, minDistance, height_min, height_max, width_min, width_max);

    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);

    cout << "Temps pour calculer aleatoirement " << nbTrees << " arbres : " << duration.count() << " millisecondes." << endl;

    return result;
}
