#include "box2.h"

Box2::Box2() : a(), b()
{
}

Box2::Box2(const Vec2& _a, const Vec2& _b) : a(_a), b(_b)
{
}

bool Box2::Inside(const Vec2& p) const
{
    return p[0] >= a[0] && p[0] <= b[0] && p[1] >= a[1] && p[1] <= b[1];
}

bool Box2::Intersect(const Box2& box) const
{
    return Inside(box.a) || Inside(box.b);
}
