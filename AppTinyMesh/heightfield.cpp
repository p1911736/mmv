#include "heightfield.h"
#include <QtCore/qfile.h>
#include <limits>

const int MAX_INTERSECTIONS = 10;
const int NUM_RAYS = 25;
const double MAX_RAY_DISTANCE = 5.0;

HeightField::HeightField()
{

}

HeightField::HeightField(const Vec2& a, const Vec2& b, int nx, int ny)
    : ScalarField(a, b, nx, ny) {
    dx = (b[0] - a[0]) / static_cast<double>(nx);
    dy = (b[1] - a[1]) / static_cast<double>(ny);
}

HeightField::HeightField(const ScalarField& sf)
    : ScalarField(sf.a, sf.b, sf.nx, sf.ny, sf.values)
{
    dx = (sf.b[0] - sf.a[0]) / static_cast<double>(sf.nx);
    dy = (sf.b[1] - sf.a[1]) / static_cast<double>(sf.ny);
}

HeightField::HeightField(const QString& heightMapFileName, int dx_, int dy_) : ScalarField(), dx(dx_), dy(dy_) {
    QImage heightMapImage(heightMapFileName);

    if (heightMapImage.isNull()) {
        qDebug() << "Erreur lors du chargement de l'image de carte de hauteur" << heightMapFileName;
        return;
    }

    const int nx = heightMapImage.width();
    const int ny = heightMapImage.height();

    if (nx <= 0 || ny <= 0) {
        qDebug() << "Dimensions de l'image de carte de hauteur non valides";
        return;
    }

    const Vec2& a = Vec2{ 0.0, 0.0 };
    const Vec2& b = Vec2{ static_cast<double>(nx * dx_), static_cast<double>(ny * dy_) };

    ScalarField::operator=(ScalarField(a, b, nx, ny));

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const QRgb pixelValue = heightMapImage.pixel(i, j);
            const double heightValue = qRed(pixelValue);
            SetValue(i, j, heightValue);
        }
    }
}

double HeightField::Height(int i, int j) const {
    return GetValue(i, j);
}

double HeightField::Slope(int i, int j) const {
    return Gradient(i, j).Length();
}

double HeightField::AverageSlope(int i, int j) const {
    double sum = 0.0;
    for (int dx = -1; dx <= 1; ++dx) {
        for (int dy = -1; dy <= 1; ++dy) {
            sum += Slope(i + dx, j + dy);
        }
    }
    return sum / 8.0;
}

Vec3 HeightField::Vertex(int i, int j) const {
    const double x = a[0] + i * dx;
    const double y = a[1] + j * dy;
    return Vec3{ x, y, Height(i, j) };
}

Vec3 HeightField::Normal(int i, int j) const {
    Vec2 gradient = Gradient(i, j);
    return Vec3{ -gradient[0], -gradient[1], 1.0 }.Normalize();
}

void HeightField::Shade(const QString& fileName, const Vec3& lightDirection) const {
    QImage image(nx, ny, QImage::Format_RGB32);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {

            const double dotProduct = max(0.0, Normal(i, j).Dot(lightDirection));

            // Pour obtenir une valeur entre 0 et 255
            const int intensity = static_cast<int>(dotProduct * 255);

            const QColor pixelColor(intensity, intensity, intensity);
            image.setPixel(i, j, pixelColor.rgb());
        }
    }
    image.save(fileName);
}

void HeightField::Export(const QString& fileName) const {
    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "Erreur lors de l'ouverture du fichier" << fileName;
        return;
    }

    QTextStream out(&file);

    // �criture des sommets
    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            Vec3 vertex = Vertex(i, j);
            out << "v " << vertex[0] << " " << vertex[1] << " " << vertex[2] << "\n";
        }
    }

    // �criture des faces (indices des sommets)
    for (int i = 0; i < nx - 1; ++i) {
        for (int j = 0; j < ny - 1; ++j) {
            const int currentIndex = i * ny + j + 1;
            const int nextIndex = (i + 1) * ny + j + 1;

            // Face triangulaire 1
            out << "f " << currentIndex << " " << nextIndex << " " << currentIndex + 1 << "\n";

            // Face triangulaire 2
            out << "f " << nextIndex << " " << nextIndex + 1 << " " << currentIndex + 1 << "\n";
        }
    }

    file.close();
}

ScalarField HeightField::StreamArea() const {
    ScalarField slope(a, b, nx, ny, vector<double>(nx * ny, 0.0));
    ScalarField streamArea(a, b, nx, ny, vector<double>(nx * ny, 1.0));

    double totalSlope = 0.0;

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            for (int neighborX = max(0, i - 1); neighborX < min(nx, i + 1); neighborX++) {
                for (int neighborY = max(0, j - 1); neighborY < min(ny, j + 1); neighborY++) {
                    if (Height(neighborX, neighborY) < Height(i, j)) {
                        totalSlope += Height(i, j) - Height(neighborX, neighborY);
                        const double newSlope = slope.GetValue(i, j) + (Height(i, j) - Height(neighborX, neighborY));
                        slope.SetValue(i, j, newSlope);
                    }
                }
            }
        }
    }

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            streamArea.SetValue(i, j, streamArea.GetValue(i, j) * (slope.GetValue(i, j) / max(1.0, abs(totalSlope))));
        }
    }

    return streamArea;
}

ScalarField HeightField::StreamPower() const {
    ScalarField slope(a, b, nx, ny, vector<double>(nx * ny, 0.0));
    ScalarField neighborRatio(a, b, nx, ny, vector<double>(nx * ny, 0.0));
    ScalarField streamPower(a, b, nx, ny, vector<double>(nx * ny, 0.0));

    double totalSlope = 0.0;

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            for (int neighborX = max(0, i - 1); neighborX < min(nx, i + 1); neighborX++) {
                for (int neighborY = max(0, j - 1); neighborY < min(ny, j + 1); neighborY++) {
                    if (Height(neighborX, neighborY) < Height(i, j)) {
                        totalSlope += Height(i, j) - Height(neighborX, neighborY);
                        const double newSlope = slope.GetValue(i, j) + (Height(i, j) - Height(neighborX, neighborY));
                        slope.SetValue(i, j, newSlope);
                        neighborRatio.SetValue(i, j, neighborRatio.GetValue(i, j) + 1);
                    }
                }
            }
        }
    }

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            const double normalizedSlope = slope.GetValue(i, j) / max(1.0, abs(totalSlope));
            streamPower.SetValue(i, j, normalizedSlope * sqrt(neighborRatio.GetValue(i, j)));
        }
    }

    return streamPower;
}

ScalarField HeightField::WetnessIndex() const {
    ScalarField streamArea = StreamArea();
    ScalarField streamPower = StreamPower();

    ScalarField wetnessIndex(a, b, nx, ny, vector<double>(nx * ny, 0.0));

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            wetnessIndex.SetValue(i, j, log(1.0 + streamArea.GetValue(i, j) / (streamPower.GetValue(i, j) + epsilon)));
        }
    }

    return wetnessIndex;
}

double HeightField::CalculateMaxSlope() const {
    double maxSlope = 0.0;

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double slope = Slope(i, j);
            maxSlope = max(maxSlope, abs(slope));
        }
    }

    return maxSlope;
}

bool HeightField::Intersect(const Ray& ray, double& t, double R) const
{
    double t1, t2;

    t1 = max(t1, 0.0);
    t2 = min(t2, R);

    t = t1;

    const Vec3 origin{ ray.Origin()[0], ray.Origin()[1], ray.Origin()[2] };
    const Vec3 direction{ ray.Direction()[0], ray.Direction()[1], ray.Direction()[2] };

    for (unsigned int i = 0 ; i < MAX_INTERSECTIONS; i++)
    {
        Vec3 p = origin + direction * t;
        const double height = Height(p[0], p[1]);

        if (height > p[2]) {
            return true;
        }

        t += max((height - p[2]) / CalculateMaxSlope(), epsilon);
    }

    return false;
}

vector<Ray> HeightField::createRaysRandomHemisphere(const Vec3& normale, int numRays, int i, int j) const {
    vector<Ray> rays;

    Vec3 direction = normale.Normalize();

    for (int k = 0; k < numRays; ++k) {
        const double theta = ((k + (rand() % 100) / 100.0) / numRays) * M_PI - (M_PI / 2.0);
        const double sigma = ((rand() % 100) / 100.0) * M_PI - (M_PI / 2.0);

        const double cos_theta = cos(theta);
        const double sin_theta = sin(theta);
        const double cos_sigma = cos(sigma);
        const double sin_sigma = sin(sigma);

        const double rotationMatrix[3][3] = {
            {cos_theta, -sin_theta, 0},
            {sin_theta * cos_sigma, cos_theta * cos_sigma, -sin_sigma},
            {sin_theta * sin_sigma, cos_theta * sin_sigma, cos_sigma}
        };

        const double rotatedX = rotationMatrix[0][0] * direction.data[0] + rotationMatrix[0][1] * direction.data[1] + rotationMatrix[0][2] * direction.data[2];
        const double rotatedY = rotationMatrix[1][0] * direction.data[0] + rotationMatrix[1][1] * direction.data[1] + rotationMatrix[1][2] * direction.data[2];
        const double rotatedZ = rotationMatrix[2][0] * direction.data[0] + rotationMatrix[2][1] * direction.data[1] + rotationMatrix[2][2] * direction.data[2];

        Vector rayDirection(rotatedX, rotatedY, rotatedZ);

        Ray ray(Vector(Vertex(i, j).data[0], Vertex(i, j).data[1], Vertex(i, j).data[2]), rayDirection);
        rays.push_back(ray);
    }

    return rays;
}

ScalarField HeightField::Access(int numRays) const {
    ScalarField access(a, b, nx, ny, vector<double>(nx * ny, 0.0));



    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            const Vec3 normale = Normal(i, j);
            const vector<Ray> rays = createRaysRandomHemisphere(normale, numRays, i, j);

            for (const Ray& ray : rays) {
                double t = 0.0;
                

                if (Intersect(ray, t, MAX_RAY_DISTANCE)) {
                    access.SetValue(i, j, access.GetValue(i, j) + 1);
                }
            }

            access.SetValue(i, j, access.GetValue(i, j) / numRays);
        }
    }

    return access;
}

void HeightField::streamPowerErosion(double deltaTime) {
    ScalarField streamArea = StreamArea();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            const double variationZT = deltaTime * sqrt(streamArea.GetValue(i, j)) * Slope(i, j);
            SetValue(i, j, GetValue(i, j) - variationZT);
        }
    }
}

void HeightField::hillSlopeErosion(double deltaTime) {
    ScalarField laplacian = Laplacian();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            const double variationZT = deltaTime * 0.1 * laplacian.GetValue(i, j);
            SetValue(i, j, GetValue(i, j) - variationZT);
        }
    }
}

double HeightField::slopeNeighbor(const int& i, const int& j, int offset_i, int offset_j) const { // On passe une valeur entre -1 et 1 pour les offsets
    // On v�rifie que le voisin avec l'offset i j existe dans le tableau
    if ((i + offset_i >= 0 && i + offset_i <= nx) && (j + offset_j >= 0 && j + offset_j <= ny)) {
        // On compare les hauteurs entre les deux points
        const double height_diff = GetValue(i + offset_i, j + offset_j) - GetValue(i, j);
        const double horiz_diff = sqrt(pow(i - (i + offset_i), 2) + pow(j - (j + offset_j), 2));

        return height_diff / horiz_diff;
    }

    return 0.0;
}

/* retourne la pente n�gatve maximale, et en parametre les indices du voisin a pente max */
double HeightField::maxSlope(const int& id_i, const int& id_j, int& i_neighbor, int& j_neighbor) const {
    double slope_max = numeric_limits<double>::infinity();
    i_neighbor = -1;
    j_neighbor = -1;

    for (int i = -1; i <= 1; i++) {
        if (id_i + i >= 0 && id_i + i < nx) {
            for (int j = -1; j <= 1; j++) {
                const double slope_tmp = slopeNeighbor(id_i, id_j, i, j);
                if (slope_tmp < slope_max) {
                    slope_max = slope_tmp;
                    i_neighbor = id_i + i;
                    j_neighbor = id_j + j;
                }
            }
        }
    }
    return slope_max;
}

void HeightField::thermalErosion(double deltaTime, double angle) {
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            // On regarde chaque pente ind�pendamment pour v�rifier qu'elle est sup�rieure � l'angle
            int max_i, max_j;
            const double max_slope = abs(maxSlope(i, j, max_i, max_j));
            if (max_slope > tan(angle)) {
                const double variationH = deltaTime * 0.01 * (max_slope - tan(angle * M_PI / 180));
                SetValue(i, j, GetValue(i, j) - variationH);

                // Replacer la matiere erod�e au voisin avec pente max :
                SetValue(max_i, max_j, GetValue(max_i, max_j) + variationH);
            }
        }
    }
}
