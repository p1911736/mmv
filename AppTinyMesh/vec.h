#ifndef VEC_H
#define VEC_H

#include <array>

using namespace std;

template <typename T, size_t N>
class Vec_ {
public:
    Vec_();
    Vec_(initializer_list<T> values);
    size_t size() const;
    T& operator[](size_t index);
    const T& operator[](size_t index) const;

    T Length() const;
    T LengthSquared() const;
    Vec_<T, N> Normalize() const;
    T Dot(const Vec_<T, N>& other) const;
    Vec_<T, N> operator+(const Vec_<T, N>& v) const;
    Vec_<T, N> operator-(const Vec_<T, N>& v) const;
    Vec_<T, N> operator*(const T& scalar) const;
    bool operator==(const Vec_<T, N>& rhs) const;
    bool operator!=(const Vec_<T, N>& rhs) const;

    array<T, N> data;
};

/* Définition des alias de type */
using Vec2 = Vec_<double, 2>;
using Vec3 = Vec_<double, 3>;

template <typename T, size_t N>
Vec_<T, N>::Vec_() : data() {}

template <typename T, size_t N>
Vec_<T, N>::Vec_(initializer_list<T> values) {
    size_t i = 0;
    for (const auto& value : values) {
        data[i++] = value;
        if (i >= N) break;
    }
}

template <typename T, size_t N>
size_t Vec_<T, N>::size() const {
    return N;
}

template <typename T, size_t N>
T& Vec_<T, N>::operator[](size_t index) {
    return data[index];
}

template <typename T, size_t N>
const T& Vec_<T, N>::operator[](size_t index) const {
    return data[index];
}

template <typename T, size_t N>
T Vec_<T, N>::Length() const {
    T sumSquared = 0;
    for (const auto& value : data) {
        sumSquared += value * value;
    }
    return sqrt(sumSquared);
}

template <typename T, size_t N>
T Vec_<T, N>::LengthSquared() const {
    T result = 0;
    for (const auto& value : data) {
        result += value * value;
    }
    return result;
}

template <typename T, size_t N>
Vec_<T, N> Vec_<T, N>::Normalize() const {
    T length = Length();
    if (length != 0) {
        Vec_<T, N> result;
        for (size_t i = 0; i < N; ++i) {
            result[i] = data[i] / length;
        }
        return result;
    }
    else {
        return *this;
    }
}

template <typename T, size_t N>
T Vec_<T, N>::Dot(const Vec_<T, N>& other) const {
    T result = 0;
    for (size_t i = 0; i < N; ++i) {
        result += data[i] * other[i];
    }
    return result;
}

template <typename T, size_t N>
Vec_<T, N> Vec_<T, N>::operator+(const Vec_<T, N>& v) const {
    Vec_<T, N> result;
    for (size_t i = 0; i < N; ++i) {
        result[i] = data[i] + v[i];
    }
    return result;
}

template <typename T, size_t N>
Vec_<T, N> Vec_<T, N>::operator-(const Vec_<T, N>& v) const {
    Vec_<T, N> result;
    for (size_t i = 0; i < N; ++i) {
        result[i] = data[i] - v[i];
    }
    return result;
}

template <typename T, size_t N>
Vec_<T, N> Vec_<T, N>::operator*(const T& scalar) const {
    Vec_<T, N> result;
    for (size_t i = 0; i < N; ++i) {
        result[i] = data[i] * scalar;
    }
    return result;
}

template <typename T, size_t N>
bool Vec_<T, N>::operator==(const Vec_<T, N>& rhs) const {
    for (size_t i = 0; i < N; ++i) {
        if (data[i] != rhs[i]) {
            return false;
        }
    }
    return true;
}

template <typename T, size_t N>
bool Vec_<T, N>::operator!=(const Vec_<T, N>& rhs) const {
    for (size_t i = 0; i < N; ++i) {
        if (data[i] == rhs[i]) {
            return false;
        }
    }
    return true;
}


#endif // VEC_H
