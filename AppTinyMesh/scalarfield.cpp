#include "scalarfield.h"

ScalarField::ScalarField() : Grid() {}

ScalarField::ScalarField(const Vec2& a, const Vec2& b, int nx, int ny)
    : Grid(a, b, nx, ny), values(nx* ny, 0.0) {}

ScalarField::ScalarField(const Vec2& a, const Vec2& b, int nx, int ny, vector<double> _values)
    : Grid(a, b, nx, ny), values(_values) {}

void ScalarField::SetValue(int i, int j, double value) {
    if (Inside(i, j)) {
        values.at(Index(i, j)) = value;
    }
}

double ScalarField::GetValue(int i, int j) const {
    if (Inside(i, j)) {
        return values.at(Index(i, j));
    }
    return -1;
}

void ScalarField::SaveAsImage(const QString& fileName, bool color) const {
    QImage image(nx, ny, QImage::Format_RGB32);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = GetValue(i, j);
            const double normalizedValue = max(0.0, min((value - a[0]) / (b[0] - a[0]), 1.0));

            QColor pixelColor;
            if (color) {
                pixelColor = QColor::fromHsvF(normalizedValue, 1.0, 1.0);
            }
            else {
                const int grayValue = static_cast<int>(normalizedValue * 255);
                pixelColor = QColor(grayValue, grayValue, grayValue);
            }

            image.setPixel(i, j, pixelColor.rgb());
        }
    }
    image.save(fileName);
}

void ScalarField::SaveAsImageStreamArea(const QString& fileName, const double& threshold, bool color) const {
    QImage image(nx, ny, QImage::Format_RGB32);

    double maxArea = 0.0;
    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            maxArea = max(maxArea, GetValue(i, j));
        }
    }

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = GetValue(i, j);
            const double normalizedValue = max(0.0, min(value / maxArea, 1.0));
            const bool isDraining = normalizedValue > threshold;

            QColor pixelColor;
            if (color) {
                if (isDraining) {
                    // Zones de drainage
                    pixelColor = QColor(Qt::blue);
                }
                else {
                    // Zones sans drainage
                    pixelColor = QColor(Qt::white);
                }
            }
            else {
                const int grayValue = isDraining ? 255 : 0;
                pixelColor = QColor(grayValue, grayValue, grayValue);
            }

            image.setPixel(i, j, pixelColor.rgb());
        }
    }
    image.save(fileName);
}

void ScalarField::SaveAsImageStreamPower(const QString& fileName, bool color) const {
    QImage image(nx, ny, QImage::Format_RGB32);

    // Trouver la valeur maximale pour normaliser
    double maxPower = 0.0;
    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            maxPower = max(maxPower, GetValue(i, j));
        }
    }

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = GetValue(i, j);
            const double normalizedValue = max(0.0, min(value / maxPower, 1.0));

            QColor pixelColor;
            if (color) {
                // Utiliser une teinte de bleu avec la saturation et la luminosit� � 1.0
                pixelColor = QColor::fromHsvF(0.66, 1.0, normalizedValue);
            }
            else {
                // Convertir en niveaux de gris
                const int grayValue = static_cast<int>(normalizedValue * 255);
                pixelColor = QColor(grayValue, grayValue, grayValue);
            }

            image.setPixel(i, j, pixelColor.rgb());
        }
    }
    image.save(fileName);
}

void ScalarField::SaveAsImageAccess(const QString& fileName, bool color) const {
    QImage image(nx, ny, QImage::Format_RGB32);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = GetValue(i, j);

            QColor pixelColor;
            if (color) {
                pixelColor = QColor::fromHsvF(value, 1.0, 1.0);
            }
            else {
                const int grayValue = static_cast<int>(value * 255);
                pixelColor = QColor(grayValue, grayValue, grayValue);
            }

            image.setPixel(i, j, pixelColor.rgb());
        }
    }
    image.save(fileName);
}


Vec2 ScalarField::Gradient(int i, int j) const {
    const double dx = (i > 0 && i < nx - 1) ? (GetValue(i + 1, j) - GetValue(i - 1, j)) / 2.0 : 0.0;
    const double dy = (j > 0 && j < ny - 1) ? (GetValue(i, j + 1) - GetValue(i, j - 1)) / 2.0 : 0.0;

    return Vec2{ dx, dy };
}


ScalarField ScalarField::GradientNorm() const {
    ScalarField gradientNorm(a, b, nx, ny);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            Vec2 gradient = Gradient(i, j);
            const double norm = sqrt(gradient[0] * gradient[0] + gradient[1] * gradient[1]);
            gradientNorm.SetValue(i, j, norm);
        }
    }

    return gradientNorm;
}


ScalarField ScalarField::Laplacian() const {
    ScalarField laplacian(a, b, nx, ny);

    for (int i = 1; i < nx - 1; ++i) {
        for (int j = 1; j < ny - 1; ++j) {
            const double laplacianValue = 4 * GetValue(i, j) -
                GetValue(i + 1, j) - GetValue(i - 1, j) -
                GetValue(i, j + 1) - GetValue(i, j - 1);
            laplacian.SetValue(i, j, laplacianValue);
        }
    }

    return laplacian;
}

ScalarField ScalarField::Smooth() const {
    ScalarField smoothed(a, b, nx, ny);

    for (int i = 1; i < nx - 1; ++i) {
        for (int j = 1; j < ny - 1; ++j) {
            // Lissage avec un masque 3x3 (1, 2, 4)
            const double smoothValue = (1 * GetValue(i, j) +
                2 * (GetValue(i + 1, j) + GetValue(i - 1, j) +
                    GetValue(i, j + 1) + GetValue(i, j - 1)) +
                4 * (GetValue(i + 1, j + 1) + GetValue(i - 1, j - 1) +
                    GetValue(i + 1, j - 1) + GetValue(i - 1, j + 1))) / 7.0;
            smoothed.SetValue(i, j, smoothValue);
        }
    }

    return smoothed;
}

ScalarField ScalarField::Blur() const {
    ScalarField blurred(a, b, nx, ny);

    for (int i = 1; i < nx - 1; ++i) {
        for (int j = 1; j < ny - 1; ++j) {
            // Lissage avec un masque 3x3 (1, 1, 1)
            const double blurValue = (1 * GetValue(i, j) +
                1 * (GetValue(i + 1, j) + GetValue(i - 1, j) +
                    GetValue(i, j + 1) + GetValue(i, j - 1)) +
                1 * (GetValue(i + 1, j + 1) + GetValue(i - 1, j - 1) +
                    GetValue(i + 1, j - 1) + GetValue(i - 1, j + 1))) / 3.0;
            blurred.SetValue(i, j, blurValue);
        }
    }

    return blurred;
}

ScalarField ScalarField::Normalize() const {
    double minValue = numeric_limits<double>::max();
    double maxValue = numeric_limits<double>::min();

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = GetValue(i, j);
            minValue = min(minValue, value);
            maxValue = max(maxValue, value);
        }
    }

    ScalarField normalized(a, b, nx, ny);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double normalizedValue = (GetValue(i, j) - minValue) / (maxValue - minValue);
            normalized.SetValue(i, j, normalizedValue);
        }
    }

    return normalized;
}

ScalarField ScalarField::Clamp(double minValue, double maxValue) const {
    ScalarField clamped(a, b, nx, ny);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = GetValue(i, j);
            const double clampedValue = clamp(value, minValue, maxValue);
            clamped.SetValue(i, j, clampedValue);
        }
    }

    return clamped;
}

ScalarField ScalarField::ApplyThreshold(double threshold) const {
    ScalarField result(*this);

    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            const double value = result.GetValue(i, j);
            result.SetValue(i, j, value > threshold ? value : 0.0);
        }
    }

    return result;
}

ostream& operator<<(ostream& os, const ScalarField& sf) {
    for (int j = 0; j < sf.ny; ++j) {
        for (int i = 0; i < sf.nx; ++i) {
            os << sf.GetValue(i, j) << " ";
        }
        os << endl;
    }
    return os;
}

void ScalarField::ScaleValues(double scale) {
    for (int i = 0; i < values.size(); ++i) {
        values[i] *= scale;
    }
}