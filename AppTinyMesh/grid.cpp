#include "grid.h"

Grid::Grid(const Vec2& a, const Vec2& b, int _nx, int _ny)
    : Box2(a, b), nx(_nx), ny(_ny)
{
}

int Grid::Index(int i, int j) const
{
    return i + nx * j;
}

bool Grid::Inside(int i, int j) const
{
    return i >= 0 && i < nx && j >= 0 && j < ny;
}