#ifndef BOX2_H
#define BOX2_H

#include <qte.h>
#include "vec.h"

class Box2
{
public:
    Box2();
    Box2(const Vec2& a, const Vec2& b);

    bool Inside(const Vec2&) const;
    bool Intersect(const Box2&) const;

    Vec2 a, b;
};

#endif
