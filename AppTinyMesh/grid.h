#ifndef GRID_H
#define GRID_H

#include "box2.h"

class Grid : public Box2 {
public:
    Grid() : Box2() {};
    Grid(const Vec2& a, const Vec2& b, int nx, int ny);

    int Index(int i, int j) const;
    bool Inside(int i, int j) const;

    int nx, ny;
};

#endif