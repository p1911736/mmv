#ifndef __Qte__
#define __Qte__

#include <QtWidgets/qmainwindow.h>
#include "realtime.h"
#include "meshcolor.h"
#include <chrono>

using namespace std;

QT_BEGIN_NAMESPACE
	namespace Ui { class Assets; }
QT_END_NAMESPACE

class HeightField;

class MainWindow : public QMainWindow
{
  Q_OBJECT
private:
  Ui::Assets* uiw;           //!< Interface

  MeshWidget* meshWidget;   //!< Viewer
  MeshColor meshColor;		//!< Mesh.

public:
  MainWindow();
  ~MainWindow();
  void CreateActions();
  void UpdateGeometry();

  HeightField* heightfield;
  HeightField *heightfieldToObj;
  QString nameObj;
  QString currentHeightFieldName;

public slots:
  void displayHeightField(QString heightField_name);
  void saveGradientNormImage();
  void saveLaplacianImage();
  void saveSmoothImage();
  void saveBlurImage();
  void saveClampImage();
  void saveShadeImage();
  void exportHeighFieldOBJ();
  void saveStreamAreaImage();
  void saveStreamPowerImage();
  void saveWetnessImage();
  void saveAccessImage();
  void createBox(int state);
  void buildVegetation();
  void applyStreamPowerErosion();
  void applyHillHopeErosion();
  void applyThermalErosion();

  void ResetCamera();
  void UpdateMaterial();
};

#endif
